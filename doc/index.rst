.. elasticgraph documentation master file

elasticgraph's documentation
==============================

|python| |pypi| |docs| |stars| |LOC| |downloads_month| |downloads_total| |license| |forks| |open issues| |project status| |repo-size|

.. include:: ../README.rst
  :start-after: .. begin-inclusion-intro-marker-do-not-remove
  :end-before: .. end-inclusion-intro-marker-do-not-remove

Getting help
------------

Having trouble? We'd like to help!

- Looking for specific information? Try the :ref:`genindex` or :ref:`modindex`.
- Report bugs with elasticgraph in our `issue tracker <https://gitlab.com/rwsdatalab/public/codebase/tools/elasticgraph/-/issues>`_.
- See this document as `pdf <elasticgraph.pdf>`_.

.. toctree::
   :maxdepth: 1
   :caption: First steps

   Installation <installation.rst>
   Usage <usage.rst>

.. toctree::
   :maxdepth: 1
   :caption: All the rest

   API <apidocs/elasticgraph.rst>
   Contributing <contributing.rst>
   License <LICENSE>
   Release notes <changelog.rst>



.. |python| image:: https://img.shields.io/pypi/pyversions/elasticgraph.svg
    :alt: |Python
    :target: https://rwsdatalab.gitlab.io/elasticgraph/

.. |pypi| image:: https://img.shields.io/pypi/v/elasticgraph.svg
    :alt: |Python Version
    :target: https://pypi.org/project/elasticgraph/

.. |docs| image:: https://img.shields.io/badge/Sphinx-Docs-blue.svg
    :alt: Sphinx documentation
    :target: https://rwsdatalab.gitlab.io/elasticgraph/

.. |stars| image:: https://img.shields.io/gitlab/stars/rwsdatalab/public/codebase/tools/elasticgraph
    :alt: Stars
    :target: https://img.shields.io/gitlab/stars/rwsdatalab/public/codebase/tools/elasticgraph


.. |LOC| image:: https://sloc.xyz/gitlab/rwsdatalab/public/codebase/tools/elasticgraph/?category=code
    :alt: lines of code
    :target: https://gitlab.com/rwsdatalab/public/codebase/tools/elasticgraph

.. |downloads_month| image:: https://static.pepy.tech/personalized-badge/elasticgraph?period=month&units=international_system&left_color=grey&right_color=brightgreen&left_text=PyPI%20downloads/month
    :alt: Downloads per month
    :target: https://pepy.tech/project/elasticgraph

.. |downloads_total| image:: https://static.pepy.tech/personalized-badge/elasticgraph?period=total&units=international_system&left_color=grey&right_color=brightgreen&left_text=Downloads
    :alt: Downloads in total
    :target: https://pepy.tech/project/elasticgraph

.. |license| image:: https://img.shields.io/badge/license-GPL3-green.svg
    :alt: License
    :target: https://gitlab.com/rwsdatalab/public/codebase/tools/elasticgraph/blob/master/LICENSE

.. |forks| image:: https://img.shields.io/gitlab/forks/rwsdatalab/public/codebase/tools/elasticgraph.svg
    :alt: gitlab Forks
    :target: https://gitlab.com/rwsdatalab/public/codebase/tools/elasticgraph/network

.. |open issues| image:: https://img.shields.io/gitlab/issues/rwsdatalab/public/codebase/tools/elasticgraph.svg
    :alt: Open Issues
    :target: https://gitlab.com/rwsdatalab/public/codebase/tools/elasticgraph/issues

.. |project status| image:: http://www.repostatus.org/badges/latest/active.svg
    :alt: Project Status
    :target: http://www.repostatus.org/#active

.. |repo-size| image:: https://img.shields.io/gitlab/repo-size/rwsdatalab/public/codebase/tools/elasticgraph
    :alt: repo-size
    :target: https://img.shields.io/gitlab/repo-size/rwsdatalab/public/codebase/tools/elasticgraph
