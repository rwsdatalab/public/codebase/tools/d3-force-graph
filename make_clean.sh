echo "Cleaning previous builds first.."
rm -rf dist
rm -rf build
rm -rf elasticgraph.egg-info
rm -rf elasticgraph/__pycache__
rm -rf elasticgraph/d3js/__pycache__
rm -rf elasticgraph/data/__pycache__
rm -rf __pycache__
rm -rf .pytest_cache
rm -rf .pylint.d
rm *.js
rm *.css
rm *.html

rm elasticgraph/*.js
rm elasticgraph/*.css
rm elasticgraph/*.html
