Changelog
=========

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[v0.1.0]
------------

Added
"""""

* Empty Python project directory structure
