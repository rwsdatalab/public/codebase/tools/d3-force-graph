elasticgraph package
====================

.. automodule:: elasticgraph
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   elasticgraph.data

Submodules
----------

.. toctree::
   :maxdepth: 4

   elasticgraph.elasticgraph
   elasticgraph.example
